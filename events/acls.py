from events import keys
import requests
import json


def get_photo(city, state):
    # headers = {"Authorization": keys.PEXELS_API_KEY}
    # urlgenerate = (
    #     "https://api.pexels.com/v1/search?=query" + city + " " + state
    # )
    # r = requests.get(urlgenerate, headers=headers)
    # j = json.loads(r.content)
    # print(j)
    # try:
    #     print("did it")
    #     return {"picture_url": j["photos"][0]["url"]}
    # except:
    #     print("failure")
    #     return {"picture_url": None}
    headers = {"Authorization": keys.PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return content["photos"][0]["url"]
    except:
        return None


def get_weather_data(city, state):
    # headers = {"Authorization": keys.OPEN_WEATHER_API_KEY}
    paramslonglat = {
        "q": city + "," + state + "," + "US",
        "appid": keys.OPEN_WEATHER_API_KEY,
    }
    urllonglat = "http://api.openweathermap.org/geo/1.0/direct"
    responselonglat = requests.get(urllonglat, params=paramslonglat)
    contentlonglat = json.loads(responselonglat.content)
    print("lookhere", contentlonglat[0]["lat"])
    paramsweather = {
        "lat": contentlonglat[0]["lat"],
        "lon": contentlonglat[0]["lon"],
        "appid": keys.OPEN_WEATHER_API_KEY,
    }
    urlweather = "https://api.openweathermap.org/data/2.5/weather"
    # urlweather = f'"https://api.openweathermap.org/data/2.5/weather?lat={contentlonglat[0]["lat"]}&lon={contentlonglat[0]["lon"]}&appid={keys.OPEN_WEATHER_API_KEY}"'

    responseweather = requests.get(urlweather, paramsweather)
    data = responseweather.json()
    contentweather = json.loads(responseweather.content)
    print("looksecond", data)
    try:
        return data["main"]["temp"]
    except:
        return None
