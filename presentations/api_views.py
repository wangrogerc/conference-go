from django.http import JsonResponse

from .models import Presentation
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"Presentations": presentations}, encoder=PresentationListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        presentation = Presentation.create(Presentation, **content)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )

    def get_extra_data(self, o):

        return {"status": str(o.status)}


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        try:
            presentation = Presentation.objects.get(id=pk)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation id"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            count, _ = Presentation.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Presentation no longer exists"},
                status=400,
            )

    else:
        content = json.loads(request.body)
        presentation = Presentation.objects.get(id=pk)
        Presentation.objects.filter(id=pk).update(**content)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )
